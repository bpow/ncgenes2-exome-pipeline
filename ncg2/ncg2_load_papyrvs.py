#!/usr/bin/env python3


import pysam
import glob
import psycopg2
import psycopg2.extras
import json
import sys
import os
import subprocess
import csv
import gzip
import math
import collections
import urllib.request
import urllib.parse
import argparse
import re
import itertools
import copy

# FIXME: this should be read from something defined by the snakemake file...
PIPELINE_VERSION = '20201201'
DEFAULT_WEBSERVICE = 'https://ws.ncg2.unc.edu/EndPoints.svc/UpdateSampleStatus/{token}/{provisionalParticipantId}/{callsetId}/{statusTypeId}'
WEBSERVICE_DEBUG_TOKEN = '5795f877-500b-4f72-b73c-087246bbbd7c'
WEBSERVICE_QC_START_CODE = '160' # defined in the webservices code

try:
    from urllib import unquote  # Python 2.X
except ImportError:
    from urllib.parse import unquote  # Python 3+


annotations_description='Allele|Annotation|Annotation_Impact|Gene_Name|Gene_ID|Feature_Type|Feature_ID|Transcript_BioType|Rank|HGVS.c|HGVS.p|cDNA.pos|CDS.pos|AA.pos|Distance|WARN'.split("|")

create_temp_table = """
CREATE TEMP TABLE load_calls (
    callset_id              INTEGER,
    var_id                  BIGINT,
    qual                    DOUBLE PRECISION,
    filter                  TEXT, -- maybe change PASS to null for space reasons
    ann                     jsonb,
    gene                    text,
    priority                integer,
    functional_anno         text,
    annotation_impact       text,
    --info              jsonb,
    clinvar_var_id          text,
    scv_with_criteria       text,
    asserted_pathogenic     boolean,
    overall_af              DOUBLE PRECISION,
    popmax_af               DOUBLE PRECISION,
    popmax                  text,
    genotype                text,
    depth                   integer,
    allele_depths           integer[],
    genotype_qual           integer,
    likelihoods             integer[]
);
"""

values_sql = """
INSERT INTO load_calls (
    callset_id, var_id, qual, filter,
    ann, gene, priority, functional_anno, annotation_impact, 
    clinvar_var_id, scv_with_criteria, asserted_pathogenic, overall_af, popmax_af, popmax,
    genotype, depth, allele_depths, genotype_qual, likelihoods)
VALUES %s
"""
values_template = """
(
    %(callset_id)s, variant_analysis.get_id_for_normvar(%(seq)s, %(pos)s, %(ref)s, %(alt)s), %(qual)s, %(filter)s,
    %(annotations)s, %(gene)s, %(priority)s, %(functional_anno)s, %(annotation_impact)s,
    %(clinvar_var_id)s, %(scv_with_criteria)s, %(clinvar_asserted_pathogenic)s, %(gnomad_af)s, %(gnomad_af_popmax)s, %(gnomad_popmax)s,
    %(genotype)s, %(depth)s, %(allele_depths)s, %(genotype_qual)s, %(likelihoods)s
)
"""

def variants2records(variants, callsets, feature_filter = None):
    for k, variant_group in itertools.groupby(variants, lambda v: (v.chrom, v.pos, v.ref, *v.alts)):
        variant = next(variant_group) # just get the initial group member
        info = variant.info
        for i, alt in enumerate(variant.alts):
            output = {
                'seq': variant.chrom,
                'pos': variant.pos,
                'ref': variant.ref,
                'alt': alt,
                'qual': variant.qual,
                'filter': ','.join(variant.filter)
            }
            if 'GNOMAD_AF' in info and not None in info['GNOMAD_AF']:
                output.update(
                    gnomad_af = info['GNOMAD_AF'][i-1],
                    gnomad_af_popmax = info['GNOMAD_AF_popmax'][i-1] if 'GNOMAD_AF_popmax' in info else 0,
                    gnomad_popmax = info['GNOMAD_popmax'][i-1] if 'GNOMAD_popmax' in info else None)
            else:
                output.update(
                    gnomad_af = 0,
                    gnomad_af_popmax = 0,
                    gnomad_popmax = None)
            
            output.update(
                clinvar_var_id = info.get('clinvar_variation_id'),
                clinvar_asserted_pathogenic = info.get('clinvar_asserted_pathogenic', 0) == '1',
                scv_with_criteria = info.get('scv_with_criteria'),
            )
            
            ann = info.get('ANN', [])
            if isinstance(ann, str):
                ann = (ann,)
            annotations = [dict(zip(annotations_description, (unquote(x) for x in a.split('|')))) for a in ann]
            if feature_filter is not None:
                annotations = [a for a in annotations if re.search(feature_filter, a['Feature_ID'])]
            output.update(annotations = psycopg2.extras.Json(annotations))
            worst_anno = annotations[0] if len(annotations) > 0 else {}
            
            priority = 5
            if output['clinvar_asserted_pathogenic']:
                priority = 1
            elif (output['gnomad_af'] <= 0.01):
                impacts = [x['Annotation_Impact'] for x in annotations]
                if "HIGH" in impacts:
                    priority = 2
                elif "MODERATE" in impacts:
                    priority = 3
                else:
                    priority = 4
            
            # todo: need to think about how homref gets represented here...
            for callset_id, sample_gt in zip(callsets, list(variant.samples.values())):
                genotype = '/'.join('.' if ai is None else str(ai) for ai in sample_gt.allele_indices)
                output.update(
                    callset_id = callset_id,
                    priority = priority,
                    genotype = genotype,
                    depth = sample_gt.get('DP'),
                    allele_depths = list(sample_gt.get('AD')),
                    genotype_qual = sample_gt.get('GQ'),
                    # FIXME-- probably should only use one of these, maybe convert or fix freebayes options to make PL
                    likelihoods = list(sample_gt.get('PL') or sample_gt.get('GL')),
                    gene = worst_anno.get('Gene_Name'),
                    functional_anno = worst_anno.get('Annotation'),
                    annotation_impact = worst_anno.get('Annotation_Impact'),
                )
                yield copy.deepcopy(output)

def exonCoveragestat2dbrow(entry, callset_id):
    return [
        callset_id,
        int(entry['start0']),
        int(entry['end']),
        int(entry['region_index']),
    ] + [entry.get('pct_gte_%d'%depth) for depth in (1,5,10,20,50)] + [
        float(entry['coverage_mean']),
        float(entry['coverage_median']),
        int(entry['coverage_min']),
        int(entry['coverage_max']),
        entry['transcript_id'],
    ]

def transcriptCoveragestat2dbrow(entry, callset_id):
    return [
        callset_id,
    ] + [entry.get('pct_gte_%d'%depth) for depth in (1,5,10,20,50)] + [
        float(entry['coverage_mean']),
        float(entry['coverage_median']),
        int(entry['coverage_min']),
        int(entry['coverage_max']),
        entry['#transcript_id'],
        entry['gene'],
        entry['protein_id'],
    ]

def check_bam_readgroups(from_filename, bam_file):
    read_groups = []
    sam_header_process = subprocess.run('''module load samtools; samtools view -H "%s"'''%bam_file,
        check=True, encoding='utf-8', shell=True, stdout=subprocess.PIPE)
    for line in sam_header_process.stdout.split('\n'):
        if line.startswith('@RG'):
            for col in line.rstrip().split('\t'):
                if col.startswith('ID:'):
                    read_groups.append(col[3:])

    if set(read_groups) != set(readgroups_from_filename):
        raise RuntimeException("readgroups from filename do not match @RG from bam file:\n  @RG: %s\n  from filename: %s"%(read_groups, readgroups_from_filename))

def metrics_data_block_records(lines):
    headers = next(lines).rstrip('\n').split('\t')
    for line in lines:
        if line == '\n':
            return
        yield collections.OrderedDict(zip(headers, line.rstrip('\n').split('\t')))


def read_metrics(lines, metrics_of_interest = 'picard.analysis.directed.HsMetrics'):
    for line in lines:
        if line.startswith('## METRICS CLASS\t'):
            metrics_class = line.rstrip().split('\t')[1]
            if metrics_class in metrics_of_interest:
                return [x for x in metrics_data_block_records(lines)]


def deliveryStatusForSample(samplename, project_code, api_key):
    params = urllib.parse.urlencode({'query.sampleName': samplename})
    req = urllib.request.Request('https://tracseq.genomics.unc.edu/api/DeliveryStatus?%s'%params,
            headers={'X-TRACSEQ-PROJECT-CODE': project_code, 'X-TRACSEQ-API-KEY': api_key})
    return json.load(urllib.request.urlopen(req))


def load_callset(cur, variant_file, sample_info):
    for sample, info in sample_info.items():
        with open(info['sex_heuristic_file']) as shf:
            inferred_sex = shf.readline().rstrip()

        cur.execute("INSERT INTO sample_analysis.callset (provisional_sample, read_groups, pipeline_version, inferred_sex) VALUES (%s, %s, %s, %s) RETURNING callset_id",
            (sample, info['readgroups_from_filename'], PIPELINE_VERSION, inferred_sex))
        info['callset_id'] = cur.fetchone()[0]

    cur.execute(create_temp_table)

    callsets = [sample_info[x]['callset_id'] for x in list(variant_file.header.samples)]

    psycopg2.extras.execute_values(cur, sql=values_sql, argslist=(x for x in variants2records(variant_file, callsets, args.feature_filter)), template=values_template, page_size=10000)

    cur.execute("""
    SELECT callset_id, count(var_id), count(distinct var_id) FROM load_calls GROUP BY callset_id;
    """)
    print("-- Callset var_id (and distinct var_id) counts:")
    for row in cur:
        print(row)

    cur.execute("""
    INSERT INTO sample_analysis.callset_call
    SELECT
        DISTINCT ON (callset_id, var_id)
        callset_id,
        var_id,
        FALSE AS discuss,
        FALSE AS verify,
        FALSE AS suspected_artifact,
        qual, filter,
        ann, gene, priority, functional_anno, annotation_impact, 
        clinvar_var_id, scv_with_criteria, asserted_pathogenic, overall_af, popmax_af, popmax,
        genotype, depth, allele_depths, genotype_qual, likelihoods
    FROM load_calls ORDER BY callset_id, var_id, priority desc, depth desc;
    """)

    print("inserted %d records"%(cur.rowcount))
    return sample_info


def load_identity_check(cur, provisional_sample, callset_id, identity_check_file):
    with pysam.VariantFile(identity_check_file) as ic_vars:
        if ic_vars.header.samples[0] != provisional_sample:
            raise RuntimeException("%s is name of sample in %s, expected %s"%(ic_vars.header.samples[0], identity_check_file, provisional_sample))
        for v in ic_vars:
            genotype = '/'.join(v.samples[0].alleles)
            depth = v.samples[0]['DP']
            allele_depths = list(v.samples[0]['AD'])
            # hacky -- works for autosomes
            chrom = str(math.floor(float(v.chrom[3:])))
            cur.execute('''
                INSERT INTO sample_analysis.identity_check (callset_id, identity_check_site_id, genotype, depth, allele_depths)
                VALUES (%s, (SELECT identity_check_site_id FROM sample_analysis.identity_check_site WHERE chrom=%s AND pos38=%s), %s, %s, %s)
                ''', (callset_id, chrom, v.pos, genotype, depth, allele_depths))


def load_coverages(cur, callset_id, exon_coverage_file, transcript_coverage_file):
    psycopg2.extras.execute_values(cur,
        sql = 'INSERT INTO sample_analysis.cds_exon_depth_of_coverage VALUES %s',
        argslist=(exonCoveragestat2dbrow(x, callset_id) for x in csv.DictReader(gzip.open(exon_coverage_file, 'rt'), dialect='excel-tab'))
    )

    psycopg2.extras.execute_values(cur,
        sql = 'INSERT INTO sample_analysis.transcript_depth_of_coverage VALUES %s',
        argslist=(transcriptCoveragestat2dbrow(x, callset_id) for x in csv.DictReader(gzip.open(transcript_coverage_file, 'rt'), dialect='excel-tab'))
    )


def load_qc_data(cur, provisional_sample, callset_id, hsmetrics_filename, readgroup_tracseq_stats):
    ## FIXME! There will be some tracseq changes needed:
    ## - add additional data (e.g., % perfect index)
    ## - keep up with readgroup names


    # the capitalized names below are the column headers from output of CollectHsMetrics
    insert_exome_qc_sql = '''
    INSERT INTO sample_analysis.exome_qc
        (callset_id, read_groups, cluster_pct, q30_pct, perfect_index_pct, read_count, mean_target_coverage, median_target_coverage,
         percent_unique, percent_mapped, percent_1x, percent_20x)
    VALUES
        (%(callset_id)s, %(READ_GROUP)s, %(percentOfRawClusters)s, %(q30Bases)s, %(percentPerfectIndexReads)s,
         %(PF_READS)s, %(MEAN_TARGET_COVERAGE)s, %(MEDIAN_TARGET_COVERAGE)s,
         %(PCT_PF_UQ_READS)s, %(PCT_PF_UQ_READS_ALIGNED)s, %(PCT_TARGET_BASES_1X)s, %(PCT_TARGET_BASES_20X)s)'''



    with open(hsmetrics_filename) as unparsed:
        metrics = read_metrics(unparsed)

    # TODO: check this against RG from bam file...
    all_readgroups = sorted([m['READ_GROUP'] for m in metrics if m['READ_GROUP'] != '' and m['SAMPLE'] == provisional_sample])

    for m in metrics:
        if m['SAMPLE'] == provisional_sample:
            m['callset_id'] = callset_id
            if m['READ_GROUP'] == '':
                if len(all_readgroups) == 1:
                    # no need for a merged record if there is only one readgroup present...
                    continue
                m['READ_GROUP'] = all_readgroups
                # TODO: consider averaging across flowcells...
                m['percentOfRawClusters'] = None
                m['q30Bases'] = None
                m['percentPerfectIndexReads'] = None
            else:
                try:
                    m.update(readgroup_tracseq_stats[m['READ_GROUP']])
                except KeyError:
                    m['percentOfRawClusters'] = None
                    m['q30Bases'] = None
                    m['percentPerfectIndexReads'] = None
                m['READ_GROUP'] = [m['READ_GROUP']]
            cur.execute(insert_exome_qc_sql, m)

    

if '__main__' == __name__:
    parser = argparse.ArgumentParser(description="load calls and qc into papyrvs")
    parser.add_argument('variant_file', nargs=1, help="File containing variants (VCF or BCF)")
    parser.add_argument('-i', '--ignore-tracseq', default=False, action='store_true')
    parser.add_argument('-b', '--ignore-bam-readgroup-check', default=False, action='store_true')
    parser.add_argument('-w', '--webservice-notification', default=False, action='store_true', help='Notify webservice of completion')
    parser.add_argument('-f', '--feature-filter', help='Regular expression by which to filter the "Feature_ID" (usually transcript name; try "^NM_")')
    parser.add_argument('-p', '--project', help='project name to be associated with sample (e.g., NCGENES2, FES, FGS)')

    args = parser.parse_args()

    variant_file = pysam.VariantFile(args.variant_file[0])
    sample_info = dict((x, {}) for x in list(variant_file.header.samples))
    for sample, info in sample_info.items():
        sex_heuristic_files = glob.glob('markdup/%s.*.sexheuristic'%sample)
        if len(sex_heuristic_files) != 1:
            raise RuntimeError("expect single sexheuristic file per sample for %s"%sample)
        info['sex_heuristic_file'] = sex_heuristic_files[0]
        callset_base = info['sex_heuristic_file'][:-len('.sexheuristic')].split('/')[-1]
        info['callset_base'] = callset_base
        info['identity_check_file'] = 'identity_check/%s.ic.vcf.gz'%callset_base
        info['exon_coverage_file'] = 'markdup/%s.exon.coveragestats.tsv.gz'%callset_base
        info['transcript_coverage_file'] = 'markdup/%s.transcript.coveragestats.tsv.gz'%callset_base
        info['sex_heuristic_file'] = 'markdup/%s.sexheuristic'%callset_base
        info['bam_file'] = 'markdup/%s.bam'%callset_base
        info['hsmetrics_file'] = 'markdup/%s.hsmetrics'%callset_base
        (info['samplename'], plus_readgroups) = callset_base.split('.', 1)
        info['readgroups_from_filename'] = sorted(plus_readgroups.split('+'))

        if not args.ignore_bam_readgroup_check:
            check_bam_readgroups(info['readgroups_from_filename'], info['bam_file'])

        readgroup_tracseq_stats = {}
        if not args.ignore_tracseq:
            # check now for needed environment variables so we don't do anything else
            try:
                tracseq_project_code = os.environ['TRACSEQ_PROJECT_CODE']
                tracseq_api_key = os.environ['TRACSEQ_API_KEY']
            except KeyError:
                raise RuntimeError('The TRACSEQ_API_KEY and TRACSEQ_PROJECT_CODE variables must be set in ' +
                    'order to get data from TracSeq... or just set "-i" to only write HsMetrics')
            info['deliveryStatus'] = deliveryStatusForSample(info['samplename'], tracseq_project_code, tracseq_api_key)

            # FIXME: consider whether this query can return more than one item in demultiplexedSamples
            for ds in info['deliveryStatus']:
                if len(ds['demultiplexedSamples']) == 1:
                    readgroup_tracseq_stats['%s_%s_L%03d'%(samplename, ds['runId'], ds['lane'])] = ds['demultiplexedSamples'][0]
                elif len(ds['demultiplexedSamples']) == 0:
                    print("%s did not have any demultiplexed samples, may have been a failed run in TracSeq"%ds)
                else:
                    raise RuntimeError('Expected one demuxedsamples per sample, runId, lane, but got %d'%len(ds['demultiplexedSamples']))
        info['readgroup_tracseq_stats'] = readgroup_tracseq_stats

    try:
        dsn = os.environ['PGDSN']
        print('using connection requested in PGDSN: "%s"'%dsn)
    except KeyError:
        print("using default database connection (override with PGDSN environment variable, if you prefer)")
        dsn = "dbname=papdev host=genomicsdb.renci.org user=bcpowell"

    con = psycopg2.connect(dsn)
    cur = con.cursor()

    sample_info = load_callset(cur, variant_file, sample_info)
    for sample, info in sample_info.items():
        callset_id = info['callset_id']
        load_identity_check(cur, sample, info['callset_id'], info['identity_check_file'])
        load_coverages(cur, callset_id, info['exon_coverage_file'], info['transcript_coverage_file'])
        load_qc_data(cur, sample, callset_id, info['hsmetrics_file'], info['readgroup_tracseq_stats'])
        if args.project:
            cur.execute('''
            INSERT INTO projects.callset_project (callset_id, project_id)
            SELECT %s, project_id
            FROM projects.project WHERE project_name = %s''', (info['callset_id'], args.project))

        con.commit()

        # notify the WFE webservice that we have a sample ready for QC
        if args.webservice_notification:
            try:
                token = os.environ['NCG2_WEBSERVICE_TOKEN']
            except KeyError:
                print("No webservice token provided, using the 'debug' token")
                token = WEBSERVICE_DEBUG_TOKEN
            try:
                res = urllib.request.urlopen(DEFAULT_WEBSERVICE.format(token=token,
                    provisionalParticipantId=sample, callsetId=callset_id,
                    statusTypeId=WEBSERVICE_QC_START_CODE))
            except urllib.request.HTTPError as err:
                print("Error with notifying webservice!")
                print('\n'.join(x.decode('utf-8') for x in err))
                raise err # re-raise now that we printed out some extra info
            if res.getcode() != 200:
                raise RuntimeError('response code from webservice was %d, should have been 200'%res.getcode())
            response_text = '\n'.join(x.decode('utf-8') for x in res)
            if not response_text.lstrip().startswith('"Success"'):
                raise RuntimeError("webservice call did not report success:\n%s"%response_text)

    con.close()
