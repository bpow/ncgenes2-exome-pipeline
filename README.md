# Explorations for pipeline development for NCGENES2

Note that the *modules* described below may be useful in general, but if
you just want to run the pipeline, you may be better off setting up
[conda](https://conda.io) and skipping down to the snakemake-wrappers section.

## Modules

Scripts to compile/install various supporting software at specific versions.
Should be compatible with [Environment Modules](http://modules.sourceforge.net)
or with [Lua modules](https://github.com/TACC/Lmod), since tcl-style modules
files are generated.

To generate the modules, change to that directory and execute:

```bash
python generate_modules.py
```

(there are options to this command to produce just one of the modules, or
to specify a different config file).

The `generate_modules.cfg` file is in python ConfigParser syntax (a very
INI-like format) that, for each module, specifies a name, version, description,
and then the commands to build the module as well as the commands to insert
into the tcl file for when the module is loaded. `appdir` gets generated from
the prefix, `name` and `version`, and these variables can be included in the
build/load commands using python's string formatting (i.e., in curly-braces).

Running this command populates the `apps` and `modulefiles` directories, which
respectively contain the built software and the environment modules files to
allow them to be loaded.

You might want to include `module use $PATH_TO_THIS_SOURCE/modules/modulefiles`
in your `.profile` or `.bashrc` in order to be able to `module load` the generated
modules.

## current production pipelines

The `ncg2` directory has the current pipeline for the NCGENES2
project, and the `psq` directory for prenatalseq. I try to keep them synchronized
to each other for the most part. The `ncg2` directory used to be named "`snakemake-modules`"
because snakemake is used for the overall workflow, and environment modules
(e.g., lmod or some equivalent) to provide dependent software and data.

There are several files in this directory:

* `runme.snakemake` - mostly a reminder to myself of the options that need to
  be set when setting off a process... In particular, it sets max number of
  concurrent jobs, tells snakemake to use conda, and to use sbatch for cluster
  submission. This process will run as long as the pipeline is in process, but
  has low cpu/memory usage itself, so it should be safe to execute on a head
  node (possibly under `screen` or `tmux` so you can step away and come back).
* `Snakefile` - this is the actual description of the pipeline in all of its
  glory.
* `cluster.json` - some setup regarding default cluster settings (partition,
  memory to request, etc.).
* `config.yaml` - general settings like which reference genome should be used.

To actually run this, you should be able to copy or soft-link these four files
into your working directory, and place your input fastq files in subdirectories
like `inputs/<sample_name>/<run_name>` (replacing `sample_name` and `run_name`,
of course). This will map reads from each run, mark duplicates (with
per-sample-run bam files in `markdup` subdirectory) and then produce
variant calls in the `calls` directory (merging runs if there were more than
one run per sample).

## Things in the "attic" (deprecated code, for historical purposes)

### shell-based pipelines

`shell` and `shell-split` contain pretty-much the same pipelines (unless I let
them get out of sync), but with the latter sourcing individual scripts for the
different steps. So the former is self-contained but is a much larger file.

Bash gets gnarly when trying to program something as complicated as this, so
while this works, I would think things would get complicated trying to maintain
this over time.

The versions of software used in these pipelines are managed by the modules
generated in the `modules/modulefiles` directory.

### snakemake-based pipeline using wrappers

The `snakemake-wrapper` directory contains an implementation of the pipeline
using [Snakemake](https://snakemake.readthedocs.io), and more specifically, 
[Snakemake Wrappers](https://snakemake-wrappers.readthedocs.org/). This is
integrated with [conda](https://conda.io), so conda takes care of reproducibility
from the standpoint of which versions of software are used. I ended up deciding
that there were enough one-off changes that I had to make to get the wrappers
to work they way we needed for NCGENES2 to abandon this approach for now, using
the `snakemake-modules` approach instead (where environmental modules are used
to handle dependent software and data versions).

