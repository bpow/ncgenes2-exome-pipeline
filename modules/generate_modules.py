#!/usr/bin/python

# Yet Another Module Maker

import os
import os.path
import subprocess
import argparse

# different capitalization in python 2 vs. 3
try:
    import ConfigParser as configparser
except:
    import configparser

modulefile_template = '''
#%Module1.0#####################################################################

set helpmsg "{name} version {version} ({description})"

## Required for "module display ..."
module-whatis "$helpmsg"

## Required for "module help ..."
proc ModulesHelp {{ }} {{
        global helpmsg
        puts stderr "\t$helpmsg\n"
}}

if {{ [ file isdirectory "{appdir}" ] }} {{
{module_commands}
}} else {{
        set helpmsg "{name} version {version} is not installed on [uname --nodename]"
        puts stderr $helpmsg
}}
'''.lstrip()

modulesdir = os.getcwd()

def create_module(name, description, version, module_commands, make_commands):
    appdir = os.path.join(modulesdir, 'apps', name, version)
    os.makedirs(appdir)
    def format_for_module(s, **args):
        return s.format(name=name, description=description, version=version,
                appdir=appdir, **args)
    module_commands = format_for_module(module_commands)
    subprocess.check_call(format_for_module(make_commands), cwd=appdir, shell=True, executable='/bin/bash')
    try:
        os.makedirs(os.path.join(modulesdir, 'modulefiles', name))
    except OSError:
        pass # OK if directory already exists...
    with open(os.path.join(modulesdir, 'modulefiles', name, version), 'w') as modulefile:
        modulefile.write(format_for_module(modulefile_template, module_commands=module_commands))

if '__main__' == __name__:
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', '-c', action='store', help="configuration file")
    parser.add_argument('--target', '-t', action='append', help="only build specified target (otherwise build everything in config file")
    parser.add_argument('--exclude', '-e', action='append', help='build targets EXCEPT those specified')
    args = parser.parse_args()

    config = configparser.ConfigParser()
    if args.config:
        config.read(args.config)
    else:
        config.read(os.path.join(os.path.dirname(__file__), 'generate_modules.cfg'))

    if args.target:
        if args.exclude:
            raise RuntimeError("You may only provide one of the '-e' or '-t' options, sorry about that...")
        sections = args.target
    else:
        sections = config.sections()
        if args.exclude:
            sections = [s for s in sections if not s in args.exclude]
    for s in sections:
        opts = dict(config.items(s))
        if os.path.isfile(os.path.join(modulesdir, 'modulefiles', opts['name'], opts['version'])):
            continue
        create_module(**opts)
