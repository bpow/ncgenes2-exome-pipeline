# ploidy file for bcftools

bcftools has a handy way to see the default ploidy file:

```
bcftools call --ploidy GRCh38?
```

The X, Y and MT chromsomes need to be changed (or really, just add the additional lines)

For GRCh38, those will be NC_000023.11, NC_000024.10 and NC_012920.1
