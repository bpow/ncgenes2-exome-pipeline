configfile: "config.yaml"

localrules: all, sexheuristic, cnv_map, splitmulti

wildcard_constraints:
    seq_date = '\d{6}',
    sequencer_id = '[^_]+',
    sequencer_run_number = '\d{4}',
    flowcell_id = '[^_]+',
    run_id = '\d{6}_[^_]+_\d{4}_[^_]+',
    sample = '[^_]*',
    sample_number = 'S\d+',
    read_number = 'R\d+',
    lane_number = 'L\d{3}',
    adapter = '[ATGC]{6,10}',
    anything = '.*'

modules = config['modules']

# conveniently from `bcftools call --ploidy GRCh38?`, but of course decrementing 1st coordinate
female_ploidies = '''
chrY 0 57227415 {SAMPLE} 0
chrM 0 16569 {SAMPLE} 1
'''.lstrip().replace(" ", "\t")

male_ploidies = '''
chrX 0 9999 {SAMPLE} 1
chrX 2781479 155701381 {SAMPLE} 1
chrY 0 57227415 {SAMPLE} 1
chrM 0 16569 {SAMPLE} 1
'''.lstrip().replace(" ", "\t")


def basename2readgroup(bn):
    return bn.split('/')[-1]

import collections
sample_readgroup_basename = collections.defaultdict(dict)

with open('samples.basenames.tsv') as f:
    for row in (line.rstrip().split('\t') for line in f):
        sample_readgroup_basename[row[0]][basename2readgroup(row[1])] = row[1]

sample_merges = ['%s.%s'%(sample, '+'.join(sorted(readgroups.keys()))) for (sample, readgroups) in sample_readgroup_basename.items()]


rule all:
    input:
        #expand('annotated-filtered/{merge}.fb.splitmulti.clinvar.gnomad.bcf', merge=sample_merges),
        expand('identity_check/{merge}.ic.vcf.gz', merge=sample_merges),
        expand('bqsr/{merge}.cram', merge=sample_merges),
        expand('markdup/{merge}.flagstat', merge=sample_merges),
        expand('markdup/{merge}.hsmetrics', merge=sample_merges),
        expand('markdup/{merge}.transcript.coveragestats.tsv.gz', merge=sample_merges),
        #fastqcs,
        #prelim_ics

rule filter:
  input:
    vcf="jannovar/{stem}.vcf.gz",
    index="jannovar/{stem}.vcf.gz.tbi"
  output:
    clinvar="annotated-filtered/{stem}.clinvar.bcf",
    clinvarAndGnomad="annotated-filtered/{stem}.clinvar.gnomad.bcf"
  params:
    # removing many tags, some redundant (AO and RO are together AD, many INFO fields that 
    # would only be relevant for multi-sample calling
    remove_fields='FORMAT/AO,FORMAT/RO,FORMAT/QA,FORMAT/QR,' +
        'INFO/AN,INFO/AO,INFO/RO,' +
        'INFO/AB,INFO/ABP,INFO/AC,INFO/AF,INFO/CIGAR,INFO/DP,INFO/DPB,INFO/DPRA,' +
        'INFO/MEANALT,INFO/MIN_DP,INFO/NS,INFO/NUMALT,INFO/TYPE,INFO/technology.Illumina',
    clinvar_fields='INFO/clinvar_variation_id,INFO/clinvar_asserted_pathogenic,INFO/scv_with_criteria',
    gnomad_fields=",".join(('INFO/GNOMAD_AC:=INFO/AC',
                            'INFO/GNOMAD_AC_POPMAX:=INFO/AC_POPMAX',
                            'INFO/GNOMAD_AF:=INFO/AF',
                            'INFO/GNOMAD_AF_POPMAX:=INFO/AF_POPMAX',
                            'INFO/GNOMAD_POPMAX:=INFO/POPMAX',
                            'INFO/GNOMAD_GC:=INFO/GC'
                            )),
    module_bcftools=modules['bcftools'],
    module_clinvar=modules['clinvar'],
    module_gnomad=modules['gnomad']

  shell:
    """
    module load {params.module_bcftools}
    module load {params.module_clinvar} # sets CLINVAR_EXTRACTED_VCF
    module load {params.module_gnomad} # sets GNOMAD_VCF

    # apparently bcftools annotate does not work from stdin... It wants its input to be indexed
    bcftools annotate -x {params.remove_fields} -a ${{CLINVAR_EXTRACTED_VCF}} \
      -c {params.clinvar_fields} -Ob -o {output.clinvar:q} {input.vcf:q}
    bcftools index {output.clinvar:q}

    bcftools annotate -e "DP<8" -a ${{GNOMAD_VCF}} -c {params.gnomad_fields} -Ob -o {output.clinvarAndGnomad:q} {output.clinvar:q}
    """

rule jannovar:
  input: "calls/{stem}.vcf.gz"
  output: 
    vcf="jannovar/{stem}.vcf.gz",
    index="jannovar/{stem}.vcf.gz.tbi"
  log: "logs/jannovar/{stem}.log"
  params:
    module_jannovar=modules['jannovar'],
    module_jannovar_data=modules['jannovar_data'],
    module_genome_for_alignment=modules['genome_for_alignment'],
    module_htslib=modules['htslib']
  shell:
    """
    module load {params.module_jannovar}
    module load {params.module_jannovar_data} # sets JANNOVAR_DATA
    module load {params.module_genome_for_alignment} # sets GENOME_FOR_ALIGNMENT
    module load {params.module_htslib} # for tabix

    jannovar annotate-vcf --show-all \
      --ref-fasta "$GENOME_FOR_ALIGNMENT" \
      --database "$JANNOVAR_DATA" \
      --input-vcf {input:q} \
      --output-vcf {output.vcf:q} \
      2> {log:q}
      #--gnomad-exomes-vcf ~/scratch/crossmap/gnomad.exomes.r3.0.2.liftovervcf.refseq38.slim.vcf.gz \
      #--vcf-annotation /home/bpow/src/clinvar-extractor/ClinVar.GRCh38.2018-07.vcf.gz:CV_:clinvar_asserted_pathogenic,rcv_accession,clinvar_variation_id,clinvarset_title,assertions_with_criteria,clinvar_scv
    echo "just need to index now..."
    tabix -p vcf {output.vcf:q}
    """

rule splitmulti:
  input:
    calls="calls/{stem}.vcf.gz"
  output:
    split="calls/{stem}.splitmulti.vcf.gz"
  params:
    module_genome_for_alignment=modules['genome_for_alignment'],
    module_bcftools=modules['bcftools']
  shell:
    """
    module load {params.module_genome_for_alignment} # sets GENOME_FOR_ALIGNMENT
    module load {params.module_bcftools}

    bcftools norm -m- -f ${{GENOME_FOR_ALIGNMENT}} -Oz {input.calls:q} > {output.split:q}.preuniq
    bcftools norm -d both -f ${{GENOME_FOR_ALIGNMENT}} -Oz {output.split:q}.preuniq > {output.split:q}
    rm {output.split:q}.preuniq

    # give extra time for the file to show up on head node
    # sleep 40
    """
    
rule freebayes:
  input:
    bam="markdup/{stem}.bam",
    bai="markdup/{stem}.bai",
    cnv_map="markdup/{stem}.cnv_map.bed"
  output: "calls/{stem}.fb.vcf.gz"
  log: 'logs/freebayes/{stem}.log'
  params:
    module_htslib=modules['htslib'],
    module_freebayes=modules['freebayes'],
    module_genome_for_alignment=modules['genome_for_alignment'],
    module_exome_beds=modules['exome_beds']
  shell:
    """
    module load {params.module_htslib}
    module load {params.module_freebayes}
    module load {params.module_genome_for_alignment} # sets GENOME_FOR_ALIGNMENT
    module load {params.module_exome_beds} # sets EXOME_PADDED_BED

    freebayes \
      -f "$GENOME_FOR_ALIGNMENT" \
      -t "$EXOME_PADDED_BED" \
      --cnv-map {input.cnv_map:q} \
      --genotype-qualities \
      --strict-vcf \
      {input.bam:q} \
      2> {log:q} \
     | bgzip > {output:q}
     """

rule call_ic:
  input:
    bam="markdup/{stem}.bam",
    bai="markdup/{stem}.bai"
  output: "identity_check/{stem}.ic.vcf.gz"
  params:
    targets=config['identity_check_bed'],
    module_htslib=modules['htslib'],
    module_freebayes=modules['freebayes'],
    module_genome_for_alignment=modules['genome_for_alignment']
  shell:
    """
    module load {params.module_htslib}
    module load {params.module_freebayes}
    module load {params.module_genome_for_alignment} # sets GENOME_FOR_ALIGNMENT

    freebayes \
      -f "$GENOME_FOR_ALIGNMENT" \
      -t {params.targets:q} \
      --report-monomorphic \
      --genotype-qualities \
      --strict-vcf \
      {input.bam:q} \
     | bgzip > {output:q}
     """

rule prelim_ic:
  input:
    bam="mapped/{stem}.bam",
    bai="mapped/{stem}.bai"
  output: "prelim_ic/{stem}.ic.vcf.gz"
  params:
    targets=config['identity_check_bed'],
    module_htslib=modules['htslib'],
    module_freebayes=modules['freebayes'],
    module_genome_for_alignment=modules['genome_for_alignment'],
  shell:
    """
    module load {params.module_htslib}
    module load {params.module_freebayes}
    module load {params.module_genome_for_alignment} # sets GENOME_FOR_ALIGNMENT

    freebayes \
      -f "$GENOME_FOR_ALIGNMENT" \
      -t {params.targets:q} \
      --report-monomorphic \
      --genotype-qualities \
      --strict-vcf \
      {input.bam:q} \
     | bgzip > {output:q}
     """

rule cram:
  input: "{dir}/{stem}.bam"
  output:
    cram="{dir}/{stem}.cram",
    index="{dir}/{stem}.cram.index"
  params:
    module_samtools=modules['samtools'],
    module_gatk_bundle=modules['gatk_bundle']
  shell:
    """
    module load {params.module_samtools}
    module load {params.module_gatk_bundle}
    
    samtools view -C -T "$GENOME_FOR_ALIGNMENT" -o {output.cram:q} {input}
    samtools index -c {output.cram:q} {output.index:q}
    """

rule bqsr:
  input: "markdup/{stem}.bam"
  output:
    bam=temp("bqsr/{stem}.bam"),
    report="bqsr/{stem}.bqsr.report"
  params:
    module_gatk=modules['gatk'],
    module_gatk_bundle=modules['gatk_bundle'],
    module_resources=modules['functionally_equivalent_resources']
  log:
    recalibrator="logs/bqsr/{stem}.log",
    apply="logs/apply-bqsr/{stem}.log"
  shell:
    """
    module load {params.module_gatk}
    module load {params.module_gatk_bundle}
    module load {params.module_resources}

    unset PYTHONPATH # FIXME: there must be a better way to handle the way that the 'gatk' script interacts with python
    
    # FIXME: known_indels is part of CCDG but is not in the gatk bundle ftp download (just the gold indels), so 
    # it would have to come from the cloud download
    gatk BaseRecalibrator \
      -R "$GENOME_FOR_ALIGNMENT" \
      -I {input:q} \
      -O {output.report:q} \
      --known-sites "$GATK_DBSNP138" \
      --known-sites "$GATK_GOLD_INDELS" \
      -L chr20 \
     2>&1 > {log.recalibrator:q}

    # to use all chromosomes to build calibration table (takes much longer...)
      #-L chr1 -L chr2 -L chr3 -L chr4 -L chr5 -L chr6 -L chr7 -L chr8 -L chr9 -L chr10 -L chr11 \
      #-L chr12 -L chr13 -L chr14 -L chr15 -L chr16 -L chr17 -L chr18 -L chr19 -L chr20 -L chr21 -L chr22 \


    gatk ApplyBQSR \
      -R "$GENOME_FOR_ALIGNMENT" \
      -I {input:q} \
      -O {output.bam:q} \
      -bqsr {output.report:q} \
      --static-quantized-quals 10 \
      --static-quantized-quals 20 \
      --static-quantized-quals 30 \
     2>&1 > {log.recalibrator:q}
    """

rule flagstat:
  input: "{dir}/{stem}.bam"
  output: "{dir}/{stem}.flagstat"
  params:
    module_samtools=modules['samtools']
  shell:
    """
    module load {params.module_samtools}
    samtools flagstat {input:q} > {output:q}
    """

rule hsmetrics:
  input: "{dir}/{stem}.bam"
  output: "{dir}/{stem}.hsmetrics"
  params:
    module_picard=modules['picard'],
    module_exome_beds=modules['exome_beds'],
    module_genome_for_alignment=modules['gatk_bundle']
  shell:
    """
    module load {params.module_picard}
    module load {params.module_exome_beds} # sets EXOME_BAIT_INTERVALS and EXOME_TARGET_INTERVALS
    module load {params.module_genome_for_alignment} # sets GENOME_FOR_ALIGNMENT

    JAVA_OPTIONS=-Xmx16g PicardCommandLine CollectHsMetrics \
      INPUT={input:q} \
      OUTPUT={output:q} \
      VALIDATION_STRINGENCY=SILENT \
      LEVEL=SAMPLE LEVEL=READ_GROUP \
      BAIT_INTERVALS="${{EXOME_BAIT_INTERVALS}}" \
      TARGET_INTERVALS="${{EXOME_TARGET_INTERVALS}}" \
      R="${{GENOME_FOR_ALIGNMENT}}"
    """

rule cnv_map:
  input: "{dir}/{stem}.sexheuristic"
  output: "{dir}/{stem}.cnv_map.bed"
  run:
    with open(input[0]) as inf:
      sex = inf.readline().rstrip()
    if sex == 'Female':
      cnv_map = female_ploidies.format(SAMPLE=wildcards.stem)
    elif sex == 'Male':
      cnv_map = male_ploidies.format(SAMPLE=wildcards.stem)
    else:
      raise RuntimeError("Expected 'Female' or 'Male' as first line of '%s', got: '%s'"%(input[0], sex))
    with open(output[0], 'w') as bedfile:
     bedfile.write(cnv_map)

rule sexheuristic:
  input: "{dir}/{stem}.bam"
  output: "{dir}/{stem}.sexheuristic"
  params:
    module_samtools=modules['samtools']
  shell:
    """
    module load {params.module_samtools}
    # FIXME - only works for "chr-style" chromosome names
    # The idea is to compare ratio reads on y to reads on x to a threshold (that works in our data for agilent v6 capture data... might need to be different in other settings)
    samtools idxstats {input:q} \
      | awk '{{ sub(/\..*/, "", $1); reads[$1] = $3; total+=$3}} END {{ print (reads["chrY"] / reads["chrX"] > 0.05) ? "Male" : "Female"; print reads["chrY"] / reads["chrX"] }}' \
      > {output:q}
    """

rule depthstat:
  input:
    bam="{dir}/{stem}.bam",
    bai="{dir}/{stem}.bai"
  output:
    transcript="{dir}/{stem}.transcript.coveragestats.tsv.gz",
    exon="{dir}/{stem}.exon.coveragestats.tsv.gz",
  params:
    module_transcript_depth_stats=modules['transcript_depth_stats'],
    module_refseq_gff=modules['refseq_gff'],
    module_htslib=modules['htslib']
  shell:
    """
    module load {params.module_transcript_depth_stats}
    module load {params.module_refseq_gff} # sets REFSEQ_GFF
    module load {params.module_htslib} # for bgzip
    
    transcriptDepthStats -g "$REFSEQ_GFF" -r -b {input.bam:q} -e {output.exon:q} -t {output.transcript:q}
    """

rule markdup:
  input:
    mapped=lambda wildcards: expand('mapped/{sample}.{read_group}.bam',
                                 sample=wildcards.sample,
                                 read_group=wildcards.merged_runs.split('+'))
  output:
    bam=temp("markdup/{sample}.{merged_runs}.bam"),
    bai=temp("markdup/{sample}.{merged_runs}.bai"),
    metrics="markdup/{sample}.{merged_runs}.merged.metrics",
  params:
    pipeline_tmp=config['temp_dir'],
    inputs=lambda wildcards, input: ' '.join('INPUT="%s"'%m for m in input.mapped),
    module_picard=modules['picard']
  shell:
    """
    module load {params.module_picard}

    JAVA_OPTIONS=-Xmx16G PicardCommandLine MarkDuplicates \
      TMP_DIR={params.pipeline_tmp} \
      VALIDATION_STRINGENCY=SILENT \
      REMOVE_DUPLICATES=false \
      {params.inputs} \
      OUTPUT="{output.bam}" \
      METRICS_FILE="{output.metrics}" \
      CREATE_INDEX=true
    """

rule bwamem_picard:
  input:
    reads=lambda wildcards: [sample_readgroup_basename[wildcards.sample][wildcards.read_group] + x for x in ('_R1_001.fastq.gz', '_R2_001.fastq.gz')]
  log:
    bwamem="logs/bwamem_picard/{sample}.{read_group}.bwamem.log",
    picard="logs/bwamem_picard/{sample}.{read_group}.picard.log"
  output:
    bam=temp("mapped/{sample}.{read_group}.bam"),
    bai=temp("mapped/{sample}.{read_group}.bai")
  threads: 16
  params:
    pipeline_tmp=config['temp_dir'],
    readgroupopt=r"-R '@RG\tID:{read_group}\tPU:{read_group}\tSM:{sample}\tPL:Illumina\tCN:UNC'",
    module_bwa=modules['bwa'],
    module_picard=modules['picard'],
    module_genome_for_alignment=modules['gatk_bundle']
  shell:
    """
    module load {params.module_bwa}
    module load {params.module_picard}
    module load {params.module_genome_for_alignment} # sets GENOME_FOR_ALIGNMENT

    bwa mem -K 100000000 -Y -v 3 -t {threads} "$GENOME_FOR_ALIGNMENT" {params.readgroupopt} {input.reads:q} 2> {log.bwamem:q} |\
      PicardCommandLine SortSam \
        TMP_DIR={params.pipeline_tmp} \
        VALIDATION_STRINGENCY=SILENT \
        SORT_ORDER=coordinate \
        MAX_RECORDS_IN_RAM=1000000 \
        CREATE_INDEX=true \
        INPUT=/dev/stdin \
        OUTPUT={output.bam:q} > {log.picard:q}
    """

