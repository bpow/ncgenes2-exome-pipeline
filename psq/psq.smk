configfile: "config.yaml"

modules = config['modules']

import glob
import collections

localrules: all, sexheuristic, cnv_map, splitmulti

wildcard_constraints:
    seq_date = '\d{6}',
    sequencer_id = '[^_]+',
    sequencer_run_number = '\d{4}',
    flowcell_id = '[^_]+',
    run_id = '\d{6}_[^_]+_\d{4}_[^_]+',
    sample = '.*?',
    sample_number = 'S\d+',
    read_number = 'R\d+',
    lane_number = 'L\d{3}',
    adapter = '[ATGC]{6,10}',
    anything = '.*'




def input_path_to_attributes(path):
    '''using {sequencer_id}_{sequencer_run_number}_{flowcell_id}_{adapter}_{lane_number} for readgroup,
       extracted from filenames that look like:
       HTSF/190221_UNC32-K00270_0149_AH3VGKBBXY/NCG-20099-00_ATGCCTAA_S101_L007_R1_001.fastq.gz'''
    (sequencer_info, basename) = path.split('/')[1:]
    attributes = {'path': path, 'sequencer_info': sequencer_info, 'basename': basename}
    attributes.update('seq_date sequencer_id sequencer_run_number flowcell_id'.split(' '), sequencer_info.split('_'))
    split_basename = basename.split('_')
    attributes.update('adapter sample_number lane_number read_number'.split(' '), split_basename[-5:-1])
    # this accounts for the possibility that the sample identifier may contain underscores
    attributes['sample_identifier'] = '_'.join(split_basename[:-5])
    return attributes

def sample_identifier_to_family(sid):
    '''This is very project specific, and tries to work around several naming variations'''
    m = re.findall('(.*?)([_-](\d|[MFP]))?$', sid)
    return m[0][0]

# like: HTSF/190221_UNC32-K00270_0149_AH3VGKBBXY/NCG-20099-00_ATGCCTAA_S101_L007_R1_001.fastq.gz
#input_attributes = [input_path_to_attributes(x) for x in glob.glob('HTSF/*/*_001.fastq.gz')]


input_template = 'HTSF/{seq_date}_{sequencer_id}_{sequencer_run_number}_{flowcell_id}/{sample}_{adapter}_{sample_number}_{lane_number}_{read_number}_001.fastq.gz'

sampleRuns = dict()
fastqcs = []
prelim_ics = []
for (seq_date, sequencer_id, sequencer_run_number, flowcell_id, sample, adapter, sample_number, lane_number, read_number) in zip(*glob_wildcards(input_template)):
    sampleRuns.setdefault(sample, {}).setdefault(f'{flowcell_id}_{lane_number}_{adapter}', []).append(input_template.format(**locals()))
    fastqcs.append(f'fastqc/{sample}/{sample}_{adapter}_{sample_number}_{lane_number}_{read_number}_001.zip')
    prelim_ics.append(f'prelim_ic/{sample}.{flowcell_id}_{lane_number}_{adapter}.ic.vcf.gz')

sample_merges = ["%s.%s"%(s, '+'.join(sorted(sr.keys()))) for (s, sr) in sampleRuns.items()]

family_merges = collections.defaultdict(list)
for sample, sr in sampleRuns.items():
    family_merges[sample_identifier_to_family(sample)].append('%s.%s'%(sample, '+'.join(sorted(sr.keys()))))

# conveniently from `bcftools call --ploidy GRCh38?`, but of course decrementing 1st coordinate
female_ploidies = '''
NC_000024.9 0 59373566 {SAMPLE} 0
NC_012920.1 0 16569 {SAMPLE} 1
NC_000024.10 0 57227415 {SAMPLE} 0
NC_012920.1 0 16569 {SAMPLE} 1
'''.lstrip().replace(" ", "\t")

male_ploidies = '''
NC_000023.10 0 60000 {SAMPLE} 1
NC_000023.10 2699520 154931043 {SAMPLE} 1
NC_000024.9 0 59373566 {SAMPLE} 1
NC_012920.1 0 16569 {SAMPLE} 1
NC_000023.11 0 9999 {SAMPLE} 1
NC_000023.11 2781479 155701381 {SAMPLE} 1
NC_000024.10 0 57227415 {SAMPLE} 1
NC_012920.1 0 16569 {SAMPLE} 1
'''.lstrip().replace(" ", "\t")


rule all:
    input:
        expand('annotated-filtered/{family}.fb.splitmulti.clinvar.gnomad.bcf', family=family_merges),
        expand('jointcalls/{family}.fb.vcf.gz', family=family_merges),
        expand('identity_check/{merge}.ic.vcf.gz', merge=sample_merges),
        expand('markdup/{merge}.cram', merge=sample_merges),
        expand('markdup/{merge}.flagstat', merge=sample_merges),
        expand('markdup/{merge}.hsmetrics', merge=sample_merges),
        expand('markdup/{merge}.transcript.coveragestats.tsv.gz', merge=sample_merges),
        #fastqcs,
        prelim_ics

rule filter_jannovar:
  input:
    vcf="jannovar/{stem}.vcf.gz",
    index="jannovar/{stem}.vcf.gz.tbi"
  output:
    clinvar="jannovar-annotated-filtered/{stem}.clinvar.bcf",
    clinvarAndGnomad="jannovar-annotated-filtered/{stem}.clinvar.gnomad.bcf"
  params:
    # removing many tags, some redundant (AO and RO are together AD, many INFO fields that 
    # would only be relevant for multi-sample calling
    remove_fields='FORMAT/AO,FORMAT/RO,FORMAT/QA,FORMAT/QR,' +
        'INFO/AN,INFO/AO,INFO/RO,' +
        'INFO/AB,INFO/ABP,INFO/AC,INFO/AF,INFO/CIGAR,INFO/DP,INFO/DPB,INFO/DPRA,' +
        'INFO/MEANALT,INFO/MIN_DP,INFO/NS,INFO/NUMALT,INFO/TYPE,INFO/technology.Illumina',
    clinvar_fields='INFO/clinvar_variation_id,INFO/clinvar_asserted_pathogenic,INFO/scv_with_criteria',
    gnomad_fields=",".join(('INFO/GNOMAD_AC:=INFO/AC',
                            'INFO/GNOMAD_AC_popmax:=INFO/AC_popmax',
                            'INFO/GNOMAD_AF:=INFO/AF',
                            'INFO/GNOMAD_AF_popmax:=INFO/AF_popmax',
                            'INFO/GNOMAD_popmax:=INFO/popmax'
                            )),
    module_bcftools=modules['bcftools'],
    module_clinvar=modules['clinvar'],
    module_gnomad=modules['gnomad']

  shell:
    """
    module load {params.module_bcftools}
    module load {params.module_clinvar} # sets CLINVAR_EXTRACTED_VCF
    module load {params.module_gnomad} # sets GNOMAD_VCF

    # apparently bcftools annotate does not work from stdin... It wants its input to be indexed
    bcftools annotate -x {params.remove_fields} -a ${{CLINVAR_EXTRACTED_VCF}} \
      -c {params.clinvar_fields} -Ob -o {output.clinvar:q} {input.vcf:q}
    bcftools index {output.clinvar:q}

    bcftools annotate -e "MAX(DP)<11 || QUAL<10" -a ${{GNOMAD_VCF}} -c {params.gnomad_fields} -Ob -o {output.clinvarAndGnomad:q} {output.clinvar:q}
    """

rule jannovar:
  input: "calls/{stem}.vcf.gz"
  output: 
    vcf="jannovar/{stem}.vcf.gz",
    index="jannovar/{stem}.vcf.gz.tbi"
  log: "logs/jannovar/{stem}.log"
  params:
    module_jannovar=modules['jannovar'],
    module_jannovar_data=modules['jannovar_data'],
    module_genome_for_alignment=modules['genome_for_alignment'],
    module_htslib=modules['htslib']
  shell:
    """
    module load {params.module_jannovar}
    module load {params.module_jannovar_data} # sets JANNOVAR_DATA
    module load {params.module_genome_for_alignment} # sets GENOME_FOR_ALIGNMENT
    module load {params.module_htslib} # for tabix

    jannovar annotate-vcf --show-all \
      --ref-fasta "$GENOME_FOR_ALIGNMENT" \
      --database "$JANNOVAR_DATA" \
      --input-vcf {input:q} \
      --output-vcf {output.vcf:q} \
      2> {log:q}
      #--gnomad-exomes-vcf ~/scratch/crossmap/gnomad.exomes.r3.0.2.liftovervcf.refseq38.slim.vcf.gz \
      #--vcf-annotation /home/bpow/src/clinvar-extractor/ClinVar.GRCh38.2018-07.vcf.gz:CV_:clinvar_asserted_pathogenic,rcv_accession,clinvar_variation_id,clinvarset_title,assertions_with_criteria,clinvar_scv
    echo "just need to index now..."
    tabix -p vcf {output.vcf:q}
    """

rule filter_snpeff:
  input:
    bcf="snpeff/{stem}.bcf",
    index="snpeff/{stem}.bcf.csi"
  output:
    clinvar="annotated-filtered/{stem}.clinvar.bcf",
    clinvarAndGnomad="annotated-filtered/{stem}.clinvar.gnomad.bcf"
  params:
    # removing many tags, some redundant (AO and RO are together AD, many INFO fields that 
    # would only be relevant for multi-sample calling
    remove_fields='FORMAT/AO,FORMAT/RO,FORMAT/QA,FORMAT/QR,' +
        'INFO/AN,INFO/AO,INFO/RO,' +
        'INFO/AB,INFO/ABP,INFO/AC,INFO/AF,INFO/CIGAR,INFO/DP,INFO/DPB,INFO/DPRA,' +
        'INFO/MEANALT,INFO/MIN_DP,INFO/NS,INFO/NUMALT,INFO/TYPE,INFO/technology.Illumina',
    clinvar_fields='INFO/clinvar_variation_id,INFO/clinvar_asserted_pathogenic,INFO/scv_with_criteria',
    gnomad_fields=",".join(('INFO/GNOMAD_AC:=INFO/AC',
                            'INFO/GNOMAD_AC_popmax:=INFO/AC_popmax',
                            'INFO/GNOMAD_AF:=INFO/AF',
                            'INFO/GNOMAD_AF_popmax:=INFO/AF_popmax',
                            'INFO/GNOMAD_popmax:=INFO/popmax'
                            )),
    module_bcftools=modules['bcftools'],
    module_clinvar=modules['clinvar'],
    module_gnomad=modules['gnomad']
  shell:
    """
    module load {params.module_bcftools}
    module load {params.module_clinvar} # sets CLINVAR_EXTRACTED_VCF
    module load {params.module_gnomad} # sets GNOMAD_VCF

    # apparently bcftools annotate does not work from stdin... It wants its input to be indexed
    bcftools annotate -x {params.remove_fields} -a ${{CLINVAR_EXTRACTED_VCF}} \
      -c {params.clinvar_fields} -Ob -o {output.clinvar:q} {input.bcf:q}
    bcftools index {output.clinvar:q}

    bcftools annotate -e "MAX(DP)<11 || QUAL<10" -a ${{GNOMAD_VCF}} -c {params.gnomad_fields} -Ob -o {output.clinvarAndGnomad:q} {output.clinvar:q}
    """

rule snpeff:
  input: "jointcalls/{stem}.vcf.gz"
  output:
    bcf="snpeff/{stem}.bcf",
    index="snpeff/{stem}.bcf.csi"
  log: "logs/snpeff/{stem}.log"
  params:
    module_htslib=modules['htslib'],
    module_bcftools=modules['bcftools'],
    module_snpeff=modules['snpeff'],
    module_human_chrom_alias=modules['human_chrom_alias']
  shell:
    """
    module load {params.module_htslib}
    module load {params.module_bcftools}
    module load {params.module_snpeff}
    module load {params.module_human_chrom_alias}
    
    unset PYTHONPATH
    
    bcftools view -e "QUAL<10" {input:q} \
      | human_chrom_alias.py -a 38 -t ensembl -f vcf -s \
      | snpEff -Xmx6g ann -no PROTEIN_PROTEIN_INTERACTION_LOCUS -no PROTEIN_STRUCTURAL_INTERACTION_LOCUS GRCh38.p7.RefSeq 2> {log:q} \
      | human_chrom_alias.py -a 38 -t refseq -f vcf -s \
      | bcftools view -Ob -o {output.bcf:q}
    
    bcftools index {output.bcf:q}
    """

rule splitmulti:
  input:
    calls="jointcalls/{stem}.vcf.gz"
  output:
    split="jointcalls/{stem}.splitmulti.vcf.gz"
  params:
    module_genome_for_alignment=modules['genome_for_alignment'],
    module_bcftools=modules['bcftools']
  shell:
    """
    module load {params.module_genome_for_alignment} # sets GENOME_FOR_ALIGNMENT
    module load {params.module_bcftools}

    bcftools norm -m- -f ${{GENOME_FOR_ALIGNMENT}} -Oz {input.calls:q} > {output.split:q}.preuniq
    bcftools norm -d both -f ${{GENOME_FOR_ALIGNMENT}} -Oz {output.split:q}.preuniq > {output.split:q}
    rm {output.split:q}.preuniq

    # give extra time for the file to show up on head node
    # sleep 40
    """

rule freebayes_multi:
  input:
    bam=lambda wildcards: expand('markdup/{merge}.bam', merge=family_merges[wildcards.family]),
    bai=lambda wildcards: expand('markdup/{merge}.bai', merge=family_merges[wildcards.family]),
    individual_cnv_map=lambda wildcards: expand('markdup/{merge}.cnv_map.bed', merge=family_merges[wildcards.family])
  output:
    calls="jointcalls/{family}.fb.vcf.gz",
    fam_cnv_map="jointcalls/{family}.cnv_map.bed",
    bamlist="jointcalls/{family}.bamlist"
  log: 'logs/freebayes_multi/{family}.log'
  params:
    module_htslib=modules['htslib'],
    module_freebayes=modules['freebayes'],
    module_genome_for_alignment=modules['genome_for_alignment'],
    module_exome_beds=modules['exome_beds']
  shell:
    """
    module load {params.module_htslib}
    module load {params.module_freebayes}
    module load {params.module_genome_for_alignment} # sets GENOME_FOR_ALIGNMENT
    module load {params.module_exome_beds} # sets EXOME_PADDED_BED

    for bam in {input.bam:q}
    do
      echo $bam
    done | tee {output.bamlist:q}

    for cnv_map in {input.individual_cnv_map:q}
    do
      cat $cnv_map
    done | tee {output.fam_cnv_map:q}

    freebayes \
      -f "$GENOME_FOR_ALIGNMENT" \
      -t "$EXOME_PADDED_BED" \
      --cnv-map {output.fam_cnv_map:q} \
      --genotype-qualities \
      --strict-vcf \
      --bam-list {output.bamlist:q} \
      2> {log:q} \
     | bgzip > {output.calls:q}
     """

rule freebayes:
  input:
    bam="markdup/{stem}.bam",
    bai="markdup/{stem}.bai",
    cnv_map="markdup/{stem}.cnv_map.bed"
  output: "calls/{stem}.fb.vcf.gz"
  log: 'logs/freebayes/{stem}.log'
  params:
    module_htslib=modules['htslib'],
    module_freebayes=modules['freebayes'],
    module_genome_for_alignment=modules['genome_for_alignment'],
    module_exome_beds=modules['exome_beds']
  shell:
    """
    module load {params.module_htslib}
    module load {params.module_freebayes}
    module load {params.module_genome_for_alignment} # sets GENOME_FOR_ALIGNMENT
    module load {params.module_exome_beds} # sets EXOME_PADDED_BED

    freebayes \
      -f "$GENOME_FOR_ALIGNMENT" \
      -t "$EXOME_PADDED_BED" \
      --cnv-map {input.cnv_map:q} \
      --genotype-qualities \
      --strict-vcf \
      {input.bam:q} \
      2> {log:q} \
     | bgzip > {output:q}
     """

rule call_ic:
  input:
    bam="markdup/{stem}.bam",
    bai="markdup/{stem}.bai"
  output: "identity_check/{stem}.ic.vcf.gz"
  params:
    targets=config['identity_check_bed'],
    module_htslib=modules['htslib'],
    module_freebayes=modules['freebayes'],
    module_genome_for_alignment=modules['genome_for_alignment']
  shell:
    """
    module load {params.module_htslib}
    module load {params.module_freebayes}
    module load {params.module_genome_for_alignment} # sets GENOME_FOR_ALIGNMENT

    freebayes \
      -f "$GENOME_FOR_ALIGNMENT" \
      -t {params.targets:q} \
      --report-monomorphic \
      --genotype-qualities \
      --strict-vcf \
      {input.bam:q} \
     | bgzip > {output:q}
     """

rule prelim_ic:
  input:
    bam="mapped/{stem}.bam",
    bai="mapped/{stem}.bai"
  output: "prelim_ic/{stem}.ic.vcf.gz"
  params:
    targets=config['identity_check_bed'],
    module_htslib=modules['htslib'],
    module_freebayes=modules['freebayes'],
    module_genome_for_alignment=modules['genome_for_alignment'],
  shell:
    """
    module load {params.module_htslib}
    module load {params.module_freebayes}
    module load {params.module_genome_for_alignment} # sets GENOME_FOR_ALIGNMENT

    freebayes \
      -f "$GENOME_FOR_ALIGNMENT" \
      -t {params.targets:q} \
      --report-monomorphic \
      --genotype-qualities \
      --strict-vcf \
      {input.bam:q} \
     | bgzip > {output:q}
     """

rule cram:
  input: "{dir}/{stem}.bam"
  output:
    cram="{dir}/{stem}.cram",
    index="{dir}/{stem}.cram.crai"
  params:
    module_samtools=modules['samtools'],
    module_genome_for_alignment=modules['genome_for_alignment']
  threads: 4
  shell:
    """
    module load {params.module_samtools}
    module load {params.module_genome_for_alignment} # sets GENOME_FOR_ALIGNMENT
    
    samtools view -O cram,embed_ref=1,nthreads={threads},reference=$GENOME_FOR_ALIGNMENT --write-index -C -o {output.cram:q} {input}
    """

rule flagstat:
  input: "{dir}/{stem}.bam"
  output: "{dir}/{stem}.flagstat"
  params:
    module_samtools=modules['samtools']
  shell:
    """
    module load {params.module_samtools}
    samtools flagstat {input:q} > {output:q}
    """

rule hsmetrics:
  input: "{dir}/{stem}.bam"
  output: "{dir}/{stem}.hsmetrics"
  params:
    module_picard=modules['picard'],
    module_exome_beds=modules['exome_beds'],
    module_genome_for_alignment=modules['genome_for_alignment']
  shell:
    """
    module load {params.module_picard}
    module load {params.module_exome_beds} # sets EXOME_BAIT_INTERVALS and EXOME_TARGET_INTERVALS
    module load {params.module_genome_for_alignment} # sets GENOME_FOR_ALIGNMENT

    JAVA_OPTIONS=-Xmx16g PicardCommandLine CollectHsMetrics \
      INPUT={input:q} \
      OUTPUT={output:q} \
      VALIDATION_STRINGENCY=SILENT \
      LEVEL=SAMPLE LEVEL=READ_GROUP \
      BAIT_INTERVALS="${{EXOME_BAIT_INTERVALS}}" \
      TARGET_INTERVALS="${{EXOME_TARGET_INTERVALS}}" \
      R="${{GENOME_FOR_ALIGNMENT}}"
    """

rule cnv_map:
  input: "{dir}/{stem}.sexheuristic"
  output: "{dir}/{stem}.cnv_map.bed"
  run:
    with open(input[0]) as inf:
      sex = inf.readline().rstrip()
    if sex == 'Female':
      cnv_map = female_ploidies.format(SAMPLE=wildcards.stem.split('.')[0])
    elif sex == 'Male':
      cnv_map = male_ploidies.format(SAMPLE=wildcards.stem.split('.')[0])
    else:
      raise RuntimeError("Expected 'Female' or 'Male' as first line of '%s', got: '%s'"%(input[0], sex))
    with open(output[0], 'w') as bedfile:
     bedfile.write(cnv_map)

rule sexheuristic:
  input: "{dir}/{stem}.bam"
  output: "{dir}/{stem}.sexheuristic"
  params:
    module_samtools=modules['samtools']
  shell:
    """
    module load {params.module_samtools}
    # FIXME - only works for refseq-accession chromosome names
    # The idea is to compare ratio reads on y to reads on x to a threshold (that works in our data for agilent v6 capture data... might need to be different in other settings)
    samtools idxstats {input:q} \
      | awk '{{ sub(/\..*/, "", $1); reads[$1] = $3; total+=$3}} END {{ print (reads["NC_000024"] / reads["NC_000023"] > 0.05) ? "Male" : "Female"; print reads["NC_000024"] / reads["NC_000023"] }}' \
      > {output:q}
    """

rule depthstat:
  input:
    bam="{dir}/{stem}.bam",
    bai="{dir}/{stem}.bai"
  output:
    transcript="{dir}/{stem}.transcript.coveragestats.tsv.gz",
    exon="{dir}/{stem}.exon.coveragestats.tsv.gz",
  params:
    module_transcript_depth_stats=modules['transcript_depth_stats'],
    module_refseq_gff=modules['refseq_gff'],
    module_htslib=modules['htslib']
  shell:
    """
    module load {params.module_transcript_depth_stats}
    module load {params.module_refseq_gff} # sets REFSEQ_GFF
    module load {params.module_htslib} # for bgzip
    
    transcriptDepthStats -g "$REFSEQ_GFF" -r -b {input.bam:q} -e {output.exon:q} -t {output.transcript:q}
    """

rule markdup:
  input:
    mapped=lambda wildcards: expand('mapped/{sample}.{read_group}.bam',
                                 sample=wildcards.sample,
                                 read_group=wildcards.merged_runs.split('+'))
  output:
    bam="markdup/{sample}.{merged_runs}.bam",
    bai="markdup/{sample}.{merged_runs}.bai",
    metrics="markdup/{sample}.{merged_runs}.merged.metrics",
  params:
    pipeline_tmp=config['temp_dir'],
    inputs=lambda wildcards, input: ' '.join('INPUT="%s"'%m for m in input.mapped),
    module_picard=modules['picard']
  shell:
    """
    module load {params.module_picard}

    JAVA_OPTIONS=-Xmx16G PicardCommandLine MarkDuplicates \
      TMP_DIR={params.pipeline_tmp} \
      VALIDATION_STRINGENCY=SILENT \
      REMOVE_DUPLICATES=false \
      {params.inputs} \
      OUTPUT="{output.bam}" \
      METRICS_FILE="{output.metrics}" \
      CREATE_INDEX=true
    """

rule bwamem_picard:
  input:
    reads=lambda wildcards: sampleRuns[wildcards.sample][wildcards.read_group]
  log:
    bwamem="logs/bwamem_picard/{sample}.{read_group}.log",
    picard="logs/bwamem_picard/{sample}.{read_group}.picard.log"
  output:
    bam=temp("mapped/{sample}.{read_group}.bam"),
    bai=temp("mapped/{sample}.{read_group}.bai")
  threads: 16
  params:
    pipeline_tmp=config['temp_dir'],
    readgroupopt=r"-R '@RG\tID:{read_group}\tPU:{read_group}\tSM:{sample}\tPL:ILLUMINA\tCN:UNC'",
    module_bwa=modules['bwa'],
    module_picard=modules['picard'],
    module_genome_for_alignment=modules['genome_for_alignment']
  shell:
    """
    module load {params.module_bwa}
    module load {params.module_picard}
    module load {params.module_genome_for_alignment} # sets GENOME_FOR_ALIGNMENT

    bwa mem -C -k 31 -K 100000000 -Y -v 3 -t {threads} "$GENOME_FOR_ALIGNMENT" {params.readgroupopt} {input.reads:q} 2> {log.bwamem:q} | \
      PicardCommandLine SortSam \
        TMP_DIR={params.pipeline_tmp} \
        VALIDATION_STRINGENCY=SILENT \
        SORT_ORDER=coordinate \
        MAX_RECORDS_IN_RAM=1000000 \
        CREATE_INDEX=true \
        INPUT=/dev/stdin \
        OUTPUT={output.bam:q} > {log.picard:q}
    """

rule fastqc:
  input:
    input_template
  output:
    html="fastqc/{sample}/{sample}_{adapter}_{sample_number}_{lane_number}_{read_number}_001.html",
    zip="fastqc/{sample}/{sample}_{adapter}_{sample_number}_{lane_number}_{read_number}_001.zip"
  params:
    dirname="fastqc/{sample}",
    module_fastqc=modules['fastqc']
  shell:
    """
    module load {params.module_fastqc}

    mkdir -p {params.dirname}
    fastqc -o {params.dirname} {input:q}
    """
